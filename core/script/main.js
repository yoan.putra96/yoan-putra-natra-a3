/* Ini adalah file main.js yang akan menjadi tempat menulis javascript */

// =============DATA===============
//   ==================================
const saltQuiz = [
  {
    question:
      "Diantara kota-kota berikut, manakah yang merupakan Ibu Kota Negara Malaysia?",
    answer: ["Jakarta", "Pnom-phen", "Kuala Lumpur"],
  },

  {
    question: "berapa jumlah provinsi yang ada di Indonesia?",
    answer: ["31 Provinsi", "33 Provinsi", "20 Provinsi"],
  },

  {
    question: "Mana kah yang termasuk lembaga Legilatif dibawah ini ?",
    answer: ["Presiden", "MA", "DPR"],
  },

  {
    question: "Berapa lama masa jabatan Presiden dan Wakil Presiden?",
    answer: ["4 Tahun", "5 tahun", "15 Tahun"],
  },

  {
    question: "Mata uang Indonesia?",
    answer: ["Rupiah", "Rupee", "peso"],
  },

  {
    question: "yang mendapatkan status sebagai Daerah Istimewa adalah?",
    answer: ["Jakarta", "Sragen", "Yogyakarta"],
  },

  {
    question: "yang memiliki julukan kota pahlawan di Indonesia adalah??",
    answer: ["Aceh", "Kudus", "Surabaya"],
  },

  {
    question: "Presiden pertama Indonesia adalah??",
    answer: ["Raden Saleh", "B.J Habibie", "Ir.Soekarno"],
  },

  {
    question: "dibawah ini merupakan negara serumpun Indonesia, kecuali",
    answer: ["Malaysia", "libya", "Singapura"],
  },

  {
    question: "yang memiliki wewenang menjaga keamanan dalam negeri adalah?",
    answer: ["TNI", "Satpol PP", "POLRI"],
  },
];

const CorrectAnswer = [2, 1, 2, 1, 0, 2, 2, 2, 1, 2];

//   =====SetQuestion====
// ========================

let currentQuestion = 0;
let currentPage = currentQuestion + 1;
let totalScore = 0;
let savedAnswer = [];
let Wrapbut = document.querySelector(".but");
let prev = document.getElementById("prev");
let next = document.getElementById("next");
let container = document.getElementsByClassName("container")[0];
let finishPage = document.querySelector(".finish");
let classScore = document.querySelector(".finish h1");
let infoDone = document.querySelector(".infoUser");
let sure = document.querySelector(".confirWraper");
let submitBtn = document.querySelector(".submit");
let footer = document.getElementById("footer");
let retry = document.querySelector(".retry");

document.addEventListener("DOMContentLoaded", function (event) {
  setQuestion();
});

function setQuestion() {
  if (saltQuiz[currentQuestion] == saltQuiz[0]) {
    prev.style.display = "none";
    next.style.margin = "auto";
  } else if (saltQuiz[currentQuestion] != saltQuiz[0]) {
    prev.style.display = "block";
    next.style.margin = "0";
    Wrapbut.style.justifyContent = "space-between";
  }

  if (saltQuiz[currentQuestion] == saltQuiz[9]) {
    next.style.display = "none";
    prev.style.margin = "auto";
  } else if (saltQuiz[currentQuestion] != saltQuiz[9]) {
    next.style.display = "block";
    prev.style.margin = "0";
  }

  if (saltQuiz[currentPage] == saltQuiz[10]) {
    submitBtn.style.display = "block";
  } else {
    submitBtn.style.display = "none";
  }

  if (saltQuiz[currentQuestion] == saltQuiz[9]) {
    next.style.display = "none";
  } else if (saltQuiz[currentQuestion] != saltQuiz[9]) {
    next.style.display = "block";
  }
  console.log(
    "currentQuestion",
    currentQuestion,
    "savedAnswer",
    savedAnswer[currentQuestion]
  );

  document.getElementById("question").innerHTML =
    saltQuiz[currentQuestion]["question"];
  document.getElementById("choicesText0").innerHTML =
    "A. " + saltQuiz[currentQuestion]["answer"][0];
  document.getElementById("choicesText1").innerHTML =
    "B. " + saltQuiz[currentQuestion]["answer"][1];
  document.getElementById("choicesText2").innerHTML =
    "C. " + saltQuiz[currentQuestion]["answer"][2];

  if (savedAnswer[currentQuestion] !== undefined)
    document.getElementById(
      "choices" + savedAnswer[currentQuestion]
    ).checked = true;
  else resetAnswer();
}

let wrapCheck = document.querySelector(".QuestFill");

function nextQuestion() {
  saveAnser();
  currentPage++;
  pageNumber.innerHTML = currentPage + "/10";
  currentQuestion++;
  if (currentQuestion > saltQuiz.length - 1) {
    alert("soal telah habis!");
    pageNumber.innerHTML = "10/10";
  }
  setQuestion();
}

function submitFinish() {
  saveAnser();
  const filled = [];
  savedAnswer.forEach((value) => filled.push(value));
  if (saltQuiz.length !== filled.length) {
    wrapCheck.style.display = "flex";
  } else {
    sure.style.display = "block";
  }
}

function check(output) {
  if (output) {
    wrapCheck.style.display = "none";
  } else {
    wrapCheck.style.display = "none";
    sure.style.display = "block";
  }
}

function cnf(result) {
  if (result) {
    stopQuiz();
  }
  sure.style.display = "none";
}

function resetAnswer() {
  if (document.querySelector('input[name="choices"]:checked'))
    document.querySelector('input[name="choices"]:checked').checked = false;
}

function prevQuestion() {
  currentPage--;
  pageNumber.innerHTML = currentPage + "/10";
  currentQuestion--;
  setQuestion();
}

function stopQuiz() {
  checkScore();
  container.style.display = "none";
  finishPage.style.display = "flex";
  footer.style.display = "none";
  if (totalScore < 7) {
    classScore.innerHTML =
      "nilai kamu " + totalScore + " , sangat tidak memuaskan!";
  } else if (totalScore < 10) {
    finishPage.style.backgroundImage = "url(/assets/images/enough.webp)";

    classScore.innerHTML = "nilai kamu " + totalScore + " , Cukup baik!";
    classScore.style.color = "green";
  } else if (totalScore == 10) {
    finishPage.style.backgroundImage = "url(/assets/images/happy.webp)";
    classScore.innerHTML =
      "Anak AJAIB! nilai kamu " + totalScore + " , Keren banget lhoooo!";
    classScore.style.color = "green";
  }
  infoDone.innerHTML =
    'Quiz telah selesai, terimakasih "' + inputUser.value + '" sudah mencoba!';
  infoDone.classList.add("done");

  footer.style.display = "none";
  retry.style.display = "flex";

  return;
}

function tryAgain(result) {
  if (result) {
    window.location.reload();
  } else {
    retry.style.display = "none";
    footer.style.display = "block";
  }
}

function saveAnser() {
  const answer = document.querySelector('input[name="choices"]:checked');
  if (answer != null) {
    savedAnswer[currentQuestion] = parseInt(answer.getAttribute("value"));
  }
}

function checkScore() {
  for (let i = 0; i < savedAnswer.length; i++) {
    if (savedAnswer[i] == CorrectAnswer[i]) {
      totalScore += 1;
    }
  }
}

let start = document.getElementById("start");
let landingPage = document.querySelector(".landing");
let prof = document.querySelector(".infoUser");
let userName = document.getElementById("profName");
let inputUser = document.getElementById("user");
let pageNumber = document.getElementById("PN");
let alertCar = document.getElementById("alrt");

start.addEventListener("click", function () {
  startFunc();
});

function startFunc() {
  if (inputUser.value.length < 4) {
    alertCar.style.display = "block";
    return;
  } else if (inputUser.value.length >= 4) {
    landingPage.style.display = "none";
    container.style.display = "block";
    prof.style.display = "flex";
  }
  userName.innerHTML = inputUser.value + "  !!";
}

inputUser.addEventListener("keyup", function () {
  alertCar.style.display = "none";
});

// =====MODE======
// =================
if (localStorage.getItem("theme") == "dark") {
  setMode(true);
}

function setMode(dark) {
  if (dark) {
    document.body.setAttribute("id", "dark");
    localStorage.setItem("theme", "dark");
  } else {
    document.body.setAttribute("id", "");
    localStorage.removeItem("theme");
  }
}
